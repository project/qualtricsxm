/**
 * @file
 *
 * Resize iframe height to fix the iframe content.
 */

(function ($) {
  'use strict';
  // Create IE + others compatible event handler
  var eventMethod = window.addEventListener ? 'addEventListener' : 'attachEvent';
  var eventer = window[eventMethod];
  var messageEvent = eventMethod === 'attachEvent' ? 'onmessage' : 'message';

  // Listen to message from survey
  eventer(messageEvent, function (e) {

    // console.log('QUALTRICS: Received message!:  ', e.data);
    // TODO e.data === 'closeQSIWindow'.

    // Just do the Frame Expansion if pixel height is passed
    $('.qualtrics_iframe').height(e.data);
    $('.qualtrics_iframe').width('100%');
    return false;

  }, false);

})(jQuery);
